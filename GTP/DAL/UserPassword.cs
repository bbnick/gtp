﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GalaxyTestProject.DAL
{
    public class UserPassword
    {
        public Guid Id { get; set; }
        public byte[] PasswordHash { get; set; }
        public byte[] PasswordSalt { get; set; }
        public int FailedAmount { get; set; }

        public Guid UserId { get; set; }
        public virtual User User { get; set; }
    }
}
