﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GalaxyTestProject.DAL
{
    public class GPContext: DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<UserPassword> UserPasswords { get; set; }

        public GPContext(DbContextOptions<GPContext> options) : base(options)
        {
            
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>()
                .HasOne(u => u.UserPassword)
                .WithOne(p => p.User)
                .HasForeignKey<UserPassword>(p => p.UserId);
        }
    }
}
