﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GalaxyTestProject.Models.InputModels
{
    public class UserInputModel
    {
        [Required(ErrorMessage = "Set name")]
        [MaxLength(16, ErrorMessage = "Max name length is 12")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Set email")]
        [DataType(DataType.EmailAddress, ErrorMessage = "Invalid email address format")]
        public string Email { get; set; }
        [Required]
        [DataType(DataType.Password, ErrorMessage = "Invalid password")]
        [MaxLength(64, ErrorMessage = "Max password length is 64")]
        public string Password { get; set; }
        [Required(ErrorMessage = "Set birthdate")]
        [DataType(DataType.Date)]
        public DateTime Birthdate { get; set; }
    }
}
