﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GalaxyTestProject.Models.InputModels
{
    public class LoginInputModel
    {
        [Required(ErrorMessage = "Set email")]
        [DataType(DataType.EmailAddress, ErrorMessage = "Invalid email address format")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Set password")]
        [MaxLength(64, ErrorMessage = "Max password length is 64")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}
