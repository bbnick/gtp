﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GalaxyTestProject.Models
{
    public class RepositoryResult<T> where T : class
    {
        public T Result { get; set; }
        public string Error { get; set; }

        public RepositoryResult(T result)
        {
            Result = result;
        }

        public RepositoryResult(string error)
        {
            Error = error;
        }
    }
}
