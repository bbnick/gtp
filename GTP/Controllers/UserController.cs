﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using GalaxyTestProject.DAL;
using GalaxyTestProject.Models;
using GalaxyTestProject.Models.InputModels;
using GalaxyTestProject.Models.ViewModels;
using GalaxyTestProject.Repositories;
using GalaxyTestProject.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace GalaxyTestProject.Controllers
{
    [Route("api/user")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserRepository _userRepository;
        private readonly IHashService _hashService;
        private readonly IMapper _mapper;

        public UserController(IUserRepository userRepo, IHashService hashService, IMapper mapper)
        {
            _userRepository = userRepo;
            _hashService = hashService;
            _mapper = mapper;
        }

        [HttpPost]
        public async Task<RepositoryResult<UserViewModel>> Create(UserInputModel inputModel)
        {
            var existsUser = await _userRepository.GetAsync(inputModel.Email);
            if (existsUser != null)
            {
                return new RepositoryResult<UserViewModel>($"User with email {inputModel.Email} is already exists");
            }

            _hashService.GenerateHash(inputModel.Password, out var hash, out var salt);

            User user = _mapper.Map<User>(inputModel);
            var userPassword = new UserPassword
            {
                PasswordHash = hash,
                PasswordSalt = salt
            };
            user.UserPassword = userPassword;

            var creationResult = await _userRepository.CreateAsync(user);

            return new RepositoryResult<UserViewModel>(_mapper.Map<UserViewModel>(creationResult));
        }

        [HttpGet]
        [Authorize]
        public async Task<IActionResult> Get()
        {
            var claim = User.Claims.FirstOrDefault(c => c.Type == "Id");

            var user = await _userRepository.GetAsync(Guid.Parse(claim.Value));

            return Content(JsonConvert.SerializeObject(_mapper.Map<UserViewModel>(user)), "application/json");
        }

        [HttpGet("unhandledException")]
        public IActionResult UnhandledException()
        {
            throw new Exception("exception");
        }

        [HttpGet("error")]
        public string Error()
        {
            try
            {
                throw new Exception();
            }
            catch
            {
                return "User not found";
            }
        }
    }
}
