﻿using GalaxyTestProject.DAL;
using GalaxyTestProject.Models;
using GalaxyTestProject.Models.InputModels;
using GalaxyTestProject.Models.ViewModels;
using GalaxyTestProject.Repositories;
using GalaxyTestProject.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace GalaxyTestProject.Controllers
{
    [Route("api/login")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        private readonly IUserRepository _userRepo;
        private readonly IHashService _hashService;
        private readonly AuthConfiguration _authConfig;

        public LoginController(IUserRepository userRepo, IHashService hashService, IOptions<AuthConfiguration> options)
        {
            _userRepo = userRepo;
            _hashService = hashService;
            _authConfig = options.Value;
        }

        [HttpPut]
        public async Task<RepositoryResult<TokenViewModel>> Login([FromBody] LoginInputModel inputModel)
        {
            var user = await _userRepo.GetAsync(inputModel.Email);

            if (user == null)
            {
                return new RepositoryResult<TokenViewModel>($"User with login {inputModel.Email} not found");
            }

            if (!_hashService.ValidateHash(inputModel.Password, user.UserPassword.PasswordHash, user.UserPassword.PasswordSalt))
            {
                user.UserPassword.FailedAmount++;
                _userRepo.Update(user);
                await _userRepo.SaveAsync();

                return new RepositoryResult<TokenViewModel>("Wrong password");
            }

            var identity = GetIdentity(user.Id);
            var token = GetToken(identity);

            return new RepositoryResult<TokenViewModel>(new TokenViewModel(token));
        }

        private string GetToken(ClaimsIdentity identity)
        {
            var now = DateTime.UtcNow;
            var jwt = new JwtSecurityToken(
                    issuer: _authConfig.Issuer,
                    audience: _authConfig.Audience,
                    notBefore: now,
                    claims: identity.Claims,
                    expires: now.Add(TimeSpan.FromMinutes(_authConfig.LifetimeMinutes)),
                    signingCredentials: new SigningCredentials(_authConfig.GetSymmetricSecurityKey(), SecurityAlgorithms.HmacSha256));
            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

            return encodedJwt;
        }

        private ClaimsIdentity GetIdentity(Guid userId)
        {

            var claims = new List<Claim>
                {
                    new Claim("Id", userId.ToString())
                };
            ClaimsIdentity claimsIdentity = new ClaimsIdentity(claims, "Token");
            return claimsIdentity;
        }
    }
}
