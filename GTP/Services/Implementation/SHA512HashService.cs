﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GalaxyTestProject.Services.Implementation
{
    public class SHA512HashService : IHashService
    {
        public void GenerateHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            using (var sha512 = new System.Security.Cryptography.HMACSHA512())
            {
                passwordHash = sha512.ComputeHash(Encoding.UTF8.GetBytes(password));
                passwordSalt = sha512.Key;
            }
        }

        public bool ValidateHash(string password, byte[] passwordHash, byte[] passwordSalt)
        {
            using (var sha512 = new System.Security.Cryptography.HMACSHA512(passwordSalt))
            {
                var hash = sha512.ComputeHash(Encoding.UTF8.GetBytes(password));
                for (int i = 0; i < hash.Length; i++)
                {
                    if (hash[i] != passwordHash[i])
                        return false;
                }
            }

            return true;
        }
    }
}
