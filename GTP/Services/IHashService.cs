﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GalaxyTestProject.Services
{
    public interface IHashService
    {
        bool ValidateHash(string password, byte[] passwordHash, byte[] passwordSalt);
        void GenerateHash(string password, out byte[] passwordHash, out byte[] passwordSalt);
    }
}
