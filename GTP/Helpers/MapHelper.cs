﻿using AutoMapper;
using GalaxyTestProject.DAL;
using GalaxyTestProject.Models.InputModels;
using GalaxyTestProject.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GalaxyTestProject.Helpers
{
    public class MapHelper: Profile
    {
        public MapHelper()
        {
            CreateMap<UserInputModel, User>();
            CreateMap<User, UserViewModel>();
        }
    }
}
