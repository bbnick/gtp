using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GalaxyTestProject.DAL;
using GalaxyTestProject.Helpers;
using GalaxyTestProject.Models;
using GalaxyTestProject.Repositories;
using GalaxyTestProject.Repositories.Implementation;
using GalaxyTestProject.Services;
using GalaxyTestProject.Services.Implementation;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;

namespace GalaxyTestProject
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            var cs = Configuration.GetSection("ConnectionStrings")["DefaultConnection"];

            services.AddDbContext<GPContext>(options => options
                .UseLazyLoadingProxies()
                .UseSqlServer(cs));

            services.Configure<AuthConfiguration>(Configuration.GetSection(nameof(AuthConfiguration)));

            var authConfiguration = Configuration.GetSection(nameof(AuthConfiguration)).Get<AuthConfiguration>();

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.RequireHttpsMetadata = false;
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateLifetime = false,
                        ValidateAudience = false,
                        ValidateIssuer = false,
                        IssuerSigningKey = authConfiguration.GetSymmetricSecurityKey()
                    };
                });

            var mConfig = new AutoMapper.MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new MapHelper());
            });
            var mapper = mConfig.CreateMapper();
            services.AddSingleton(mapper);

            services.AddTransient<IUserRepository, UserRepository>();
            services.AddTransient<IHashService, SHA512HashService>();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "GalaxyTestProject API", Version = "v1" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "GalaxyTestProject API V1");
                c.RoutePrefix = "api/v1";
            });

            SetupDatabase(app);
        }

        private void SetupDatabase(IApplicationBuilder app)
        {
            using (var serviceScope = app.ApplicationServices
                .GetRequiredService<IServiceScopeFactory>()
                .CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetService<GPContext>();
                context.Database.Migrate();
            }
        }
    }
}
